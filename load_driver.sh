#! /bin/sh

# this will unbind the default hid driver loaded for the usb footswitch


is_module=`lsmod | grep footswitch`

if test is_module != ""
then
    rmmod footswitch.ko
fi

echo -n "0003:0426:3011.000A" > /sys/bus/hid/drivers/hid-generic/unbind
modprobe -r usbhid
insmod src/footswitch.ko
modprobe usbhid


