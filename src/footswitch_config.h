/*!
 * \author  maxime fournier
 * \file    footswitch_config.h
 * \date    Fri 25 Oct 2013 10:11:20 AM UTC
 */

#ifndef _FOOTSWITCH_CONFIG_H_
# define _FOOTSWITCH_CONFIG_H_

# define VENDOR_ID 0x0426
// Change it depending on your device
# define PRODUCT_ID 0x3011

// Change it depending on the number of pedals on your device.
# define FS_PEDALS 3


/*!
 * structure holding keycodes for the footswtich driver
 * replace keycode with the proper values
 * Those values are listed in linux/include/linux/input.h or
 * linux/include/uapi/linux/input.h for latest kernels
 */
static const unsigned char                          usb_fs_keycode[FS_PEDALS] =
{
    63, 65, 64
};



#endif /* !_FOOTSWITCH_CONFIG_H_ */
