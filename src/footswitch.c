/*!
 * \author  Maxime Fournier
 * \file    footswitch.c
 * \date    Mon 03 Jun 2013 11:15:48 AM UTC
 *
 * This driver is intended to be used with the scythe usb footswitch II
 */


#include <linux/module.h>
#include <linux/kernel.h>   /* printk() */
#include <linux/init.h>
#include <linux/slab.h>     /* kmalloc() */
#include <linux/usb.h>
#include <linux/usb/input.h>
#include <linux/input.h>
#include <linux/hid.h>
#include <linux/interrupt.h>
#include "footswitch_config.h"

#define DRIVER_AUTHOR "Maxime Fournier"
#define DRIVER_DESC "Scyte usb footswitch driver"


/*!
 * Global variables forward declaration
 */
static DECLARE_WAIT_QUEUE_HEAD(urb_wait_queue);
MODULE_DEVICE_TABLE(usb, id_table);
static struct usb_driver                            footswitch_driver;
static struct usb_class_driver                      driver_usb_class;
int                                                 major = 1;


/*!
 * this structure does represent the device
 */
struct usb_footswitch
{
    struct input_dev*                               dev;
    struct usb_device*                              usbdev;
    unsigned int                                    urb;
    unsigned char*                                  new;
    unsigned char                                   old[8];
    char                                            name[128];
    char                                            phys[64];
    struct usb_interface*                           interface;
    struct urb*                                     irq;
    dma_addr_t                                      new_dma;
};


/*!
 * This function actually triggers the software interupt to notify a key/pedal
 * has been pressed
 */
static void
usb_footswitch_irq(struct urb*                      urb)
{
    struct usb_footswitch*                          footswitch;
    int                                             i;

    footswitch = urb->context;
    switch (urb->status)
    {
        case 0:
            break;
        case -ECONNRESET:
        case -ENOENT:
        case -ESHUTDOWN:
            return;
        default:
            goto resubmit;
    }

    for (i = 2;
         i < 8;
         i++)
    {
        if (footswitch->old[i] > 3 && memscan(footswitch->new + 2,
                                              footswitch->old[i],
                                              6) == footswitch->new + 8)
            // footswitch->new + 8 = lim of the mem area
        {
            // -30 because of footswitch keycode, this way it triggers 0 1 2
            // index in the usb_fs_keycode array
            if (usb_fs_keycode[footswitch->old[i] - 30])
            {
                // actually trigger the interuption for the kernel
                // software interupt
                input_report_key(footswitch->dev,
                                 usb_fs_keycode[footswitch->old[i] - 30],
                                 0);
            }
            else
            {
                hid_info(urb->dev,
                         "Unknown key (scancode %#x) released.\n",
                         footswitch->old[i]);
            }
        }
        if (footswitch->new[i] > 3 && memscan(footswitch->old + 2,
                                              footswitch->new[i],
                                              6) == footswitch->old + 8)
        {
            if (usb_fs_keycode[footswitch->new[i] - 30])
            {
                input_report_key(footswitch->dev,
                                 usb_fs_keycode[footswitch->new[i] - 30],
                                 1);
            }
            else
            {
                hid_info(urb->dev,
                         "Unknown key (scancode %#x) released.\n",
                         footswitch->new[i]);
            }
        }
    }
    input_sync(footswitch->dev);
    memcpy(footswitch->old,
           footswitch->new,
           8);

resubmit:
    i = usb_submit_urb(urb,
                       GFP_ATOMIC);
    if (i)
    {
        hid_err(urb->dev,
                "can't resubmit intr, %s-%s/input0, status %d",
                footswitch->usbdev->bus->bus_name,
                footswitch->usbdev->devpath,
                i);
    }
}

/*!
 * This method opens the device and submit the urbs
 */
static int
footswitch_open(struct input_dev*                   indev)
{
    struct usb_footswitch*                          dev;

    dev = input_get_drvdata(indev);
    dev->irq->dev = dev->usbdev;
    if (usb_submit_urb(dev->irq, GFP_KERNEL))
    {
        return -EIO;
    }
    return 0;
}


/*!
 * Close method killing the urbs
 */
static void
footswitch_close(struct input_dev*                  indev)
{
    struct usb_footswitch*                          dev;

    dev = input_get_drvdata(indev);
    usb_kill_urb(dev->irq);
}


/*!
 * Allocates memory for usb device
 */
static int
footswitch_alloc_mem(struct usb_device*             dev,
                     struct usb_footswitch*         footswitch)
{
    if (!(footswitch->irq = usb_alloc_urb(0,
                                          GFP_KERNEL)))
    {
        return -1;
    }
    if (!(footswitch->new = usb_alloc_coherent(dev,
                                               8,
                                               GFP_ATOMIC,
                                               &footswitch->new_dma)))
    {
        return -1;
    }
    return 0;
}


/*!
 * Free memory for usb device
 */
static void
footswitch_free_mem(struct usb_device*              dev,
                    struct usb_footswitch*          footswitch)
{
    usb_free_urb(footswitch->irq);
    usb_free_coherent(dev,
                      8,
                      footswitch->new,
                      footswitch->new_dma);
}


/*!
 * Probe function that perform initialization work for the device.
 */
static int
footswitch_probe(struct usb_interface*              iface,
                 const struct usb_device_id*        id)
{
    // todo probe initialization but nothing to do here for now
    struct usb_footswitch*                          footswitch;
    struct usb_device*                              usbdev;
    struct usb_host_interface*                      interface;
    struct usb_endpoint_descriptor*                 endpoint;
    struct input_dev*                               input_device;
    int                                             i;
    int                                             pipe;
    int                                             maxp;
    int                                             error = -ENOMEM;

    usbdev = interface_to_usbdev(iface);
    interface = iface->cur_altsetting;
    if (interface->desc.bNumEndpoints != 1)
    {
        return -ENODEV;
    }

    endpoint = &interface->endpoint[0].desc;

    if (!usb_endpoint_is_int_in(endpoint))
    {
        return -ENODEV;
    }

    pipe = usb_rcvintpipe(usbdev,
                          endpoint->bEndpointAddress);
    maxp = usb_maxpacket(usbdev,
                         pipe,
                         usb_pipeout(pipe));
    footswitch = kzalloc(sizeof(struct usb_footswitch),
                         GFP_KERNEL);
    input_device = input_allocate_device();

    if (!footswitch || !input_device)
    {
        goto fail;
    }

    if (footswitch_alloc_mem(usbdev,
                             footswitch))
    {
        goto failalloc;
    }
    footswitch->usbdev = usbdev;
    footswitch->dev = input_device;

    if (usbdev->manufacturer)
    {
        strlcpy(footswitch->name,
                usbdev->manufacturer,
                sizeof(footswitch->name));
    }

    if (usbdev->product)
    {
        if (usbdev->manufacturer)
        {
            strlcat(footswitch->name,
                    " ",
                    sizeof(footswitch->name));
        }
        strlcat(footswitch->name,
                usbdev->product,
                sizeof(footswitch->name));
    }
    // in case nothing worked, retrieve device id directly from
    // usb core functions
    if (!strlen(footswitch->name))
    {
        snprintf(footswitch->name,
                 sizeof(footswitch->name),
                 "USB FOOTSWITCH Keyboard %04x:%04x",
                 le16_to_cpu(usbdev->descriptor.idVendor),
                 le16_to_cpu(usbdev->descriptor.idProduct));
    }
    usb_make_path(usbdev,
                  footswitch->phys,
                  sizeof(footswitch->phys));
    strlcat(footswitch->phys,
            "/input0",
            sizeof(footswitch->phys));
    input_device->name = footswitch->name;
    input_device->phys = footswitch->phys;
    usb_to_input_id(usbdev,
                    &input_device->id);
    input_device->dev.parent = &iface->dev;
    input_set_drvdata(input_device,
                      footswitch);
    input_device->evbit[0] = BIT_MASK(EV_KEY) | BIT_MASK(EV_REP);

    for (i = 0;
         i < FS_PEDALS; // Used for the length of usb_fs_keycode
         i++)
    {
        set_bit(usb_fs_keycode[i],
                input_device->keybit);
    }
    clear_bit(0,
              input_device->keybit);
    input_device->open = footswitch_open;
    input_device->close = footswitch_close;
    usb_fill_int_urb(footswitch->irq,
                     usbdev,
                     pipe,
                     footswitch->new,
                     (maxp > 8 ? 8 : maxp),
                     usb_footswitch_irq,
                     footswitch,
                     endpoint->bInterval);
    footswitch->irq->transfer_dma = footswitch->new_dma;
    footswitch->irq->transfer_flags |= URB_NO_TRANSFER_DMA_MAP;

    error = input_register_device(footswitch->dev);
    if (error)
    {
        goto failalloc;
    }
    usb_set_intfdata(iface,
                     footswitch);
    device_set_wakeup_enable(&usbdev->dev,
                             1);
    return 0;

failalloc:
    footswitch_free_mem(usbdev,
                        footswitch);
fail:
    input_free_device(input_device);
    kfree(footswitch);
    return error;
}


/*!
 * Disconnect function that is called when device is disconnected
 */
static void
footswitch_disconnect(struct usb_interface*         interface)
{
    struct usb_footswitch*                          dev;

    dev = usb_get_intfdata(interface);
    usb_deregister_dev(interface,
                       &driver_usb_class);
    usb_set_intfdata(interface,
                     NULL);
    kfree(dev);
    dev_info(&interface->dev,
             "Footswitch is now disconnected\n");
}


/*!
 * Init function called on driver loading
 */
static int __init
usb_init(void)
{
    int                                             retval = 0;

    retval = usb_register(&footswitch_driver);
    if (retval)
    {
        printk(KERN_WARNING "footswitch driver: error code while registering: %d",
               retval);
    }
    printk(KERN_INFO "footswitch driver: loaded with major %d\n",
           major);
    return retval;
}


/*!
 * Exit function called on driver unloading
 */
static void __exit
usb_footswitch_exit(void)
{
    usb_deregister(&footswitch_driver);
    printk(KERN_INFO "footswitch driver: successfully unloaded\n");
}


/*!
 * This structure holds the devices id: VENDOR_ID and PRODUCT_ID
 */
static struct usb_device_id
id_table[] =
{
    {USB_DEVICE(VENDOR_ID, PRODUCT_ID)},
    {},
};


/*!
 * This structure represents the driver and holds basic standard function
 * pointers to the id_table, probe, and disconnect method.
 * This is for driver level procedures
 */
static struct usb_driver
footswitch_driver =
{
    .name = "footswitch",
    .probe = footswitch_probe,
    .disconnect = footswitch_disconnect,
    .id_table = id_table
};


/*!
 * This structure holds the driver file operations structures and the
 * minor_base
 */
static struct usb_class_driver
driver_usb_class =
{
    .name = "footswitch%d",
    .minor_base = 0
};


/*!
 * Procedures to call for driver module initialization and exit
 */
module_init(usb_init);
module_exit(usb_footswitch_exit);

MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_LICENSE("GPL");

