obj-m += src/footswitch.o

default:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
	rm -f .*.cmd
	rm -f *.ko
	rm -f *.symvers
	rm -f *.mod.*
	rm -f *.order
	rm -f *.o
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean

